package server;

import com.google.gson.Gson;
import data.*;
import handler.*;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.PathHandler;
import io.undertow.util.HttpString;
import io.undertow.util.Methods;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import org.xnio.ChannelListener;

import java.io.IOException;
import java.util.*;

public class Server {
    private Undertow server;
    private volatile Gson gson = new Gson();

    public Server() {
        HttpHandler handler = new PathHandler(Handlers.path().
                addExactPath("/login", new Login()).
                addExactPath("/logout", new Logout()).
                addPrefixPath("/users", new GetUsers())).
                addPrefixPath("/messages", Handlers.websocket(new WebSocketConnectionCallback() {
                    @Override
                    public void onConnect(WebSocketHttpExchange webSocketHttpExchange, WebSocketChannel webSocketChannel) {

                        Data.setMessageListener(msg -> {
                            String json = gson.toJson(msg);
                            for (WebSocketChannel session : webSocketChannel.getPeerConnections()) {
                                WebSockets.sendText(json, session, null);
                            }
                        });
                        webSocketChannel.getReceiveSetter().set(new AbstractReceiveListener() {
                            @Override
                            protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) throws IOException {
                                String data = message.getData();
                                Message test = gson.fromJson(data, Message.class);
                                Data.getInstance().addMessage(test.getMessage(), test.getAuthorName());
                                String json = gson.toJson(test);
                                for (WebSocketChannel ws : channel.getPeerConnections()) {
                                    WebSockets.sendText(json, ws, null);
                                }
                            }
                        });
                        webSocketChannel.resumeReceives();

                        String userToken = webSocketHttpExchange.getRequestHeader("Authorization").replace("Token ", "");
                        Data.getInstance().setUserAddress(webSocketChannel.getPeerAddress().toString(), userToken);
                        System.out.println(webSocketChannel.getPeerAddress().toString());
                        webSocketChannel.addCloseTask(new ChannelListener<WebSocketChannel>() {
                            @Override
                            public void handleEvent(WebSocketChannel webbSocketChannel) {
                                Data.getInstance().userDisconnected(webSocketChannel.getPeerAddress().toString());
                            }
                        });
                    }
                }, new GetMessages()));
        /*HttpHandler httpHandler = (httpServerExchange) -> {
                new MyHttpHandler().
                        getHandler(httpServerExchange.getRelativePath(), httpServerExchange.getRequestMethod()).
                        handleRequest(httpServerExchange);
             };*/
        server = Undertow.builder().addHttpListener(8080, "localhost").setHandler(handler).build();
        server.start();
    }


/*
    private class MyHttpHandler {
        private RequestHandler getHandler(String path, HttpString method) {
            RequestHandler handler = unknownMethodHandler;
            if (method.equals(Methods.POST)) {
                handler = executePOST(path);
            } else if (method.equals(Methods.GET)) {
                handler = executeGET(path);
            }
            return handler;
        }

        private RequestHandler executeGET(String path) {
            String[] split = path.split("/");
            if (split[1].equals(Commands.USERS) && split.length == 3) {
                return new GetUserID(split[2]);
            } else if (split[1].equals(Commands.USERS)) {
                return new GetUsers();
            } else if(split[1].equals(Commands.MESSAGES)) {
                return new GetMessages();
            }
            return unknownRequestHandler;
        }

        private RequestHandler executePOST(String path) {
            String[] split = path.split("/");
            if (split[1].equals(Commands.LOGIN)) {
                return new Login();
            } else if (split[1].equals(Commands.LOGOUT)) {
                return new Logout();
            } else if (split[1].equals(Commands.MESSAGES)) {
                return new PostMessage();
            }
            return unknownRequestHandler;
        }


        private RequestHandler unknownMethodHandler = new RequestHandler() {
            @Override
            public void handleRequest(HttpServerExchange exchange) {
                exchange.setStatusCode(405);
                exchange.getResponseSender().send("");
            }
        };

        private RequestHandler unknownRequestHandler = new RequestHandler() {
            @Override
            public void handleRequest(HttpServerExchange exchange) {
                exchange.setStatusCode(400);
                exchange.getResponseSender().send("");
            }
        };
    }
*/
}