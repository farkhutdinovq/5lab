public interface Commands {
    String LOGIN = "login";
    String LOGOUT = "logout";
    String USERS = "users";
    String MESSAGES = "messages";
}
