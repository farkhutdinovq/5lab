package handler;

import data.Authorize;
import data.Data;
import data.Message;
import data.User;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.sse.ServerSentEventConnectionCallback;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import server.Server;

public class Login extends RequestHandler {

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {

            Authorize username = readGson(data);
            if (username == null) {
                answerOnError(exchange, 400);
                return;
            }
            if (username.getUsername() != null) {
                if (Data.getInstance().getUsers().stream().anyMatch(user -> user.getName().
                        equals(username.getUsername()) && user.isOnline())) {
                    answerUsernameInUse(exchange);
                } else {
                    User user = Data.getInstance().addUser(username.getUsername());
                    Message message = new Message(user.getName() + " joined the chat", "Chat");
                    Data.getInstance().addServerMessage(user.getName() + " joined the chat");
                    exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                    exchange.getResponseSender().
                            send(gson.toJson(new User(user.getId(), user.getName(), user.isOnline(), user.getToken())));
                }
            } else {
                answerOnError(exchange, 400);
            }
        });
    }

    private Authorize readGson(byte[] json) {
        try {
            return gson.fromJson(new String(json), Authorize.class);
        } catch (RuntimeException e) {
            return null;
        }
    }

    private void answerUsernameInUse(HttpServerExchange exchange) {
        exchange.getResponseHeaders().add(new HttpString("WWW-Authenticate"),
                "Token realm='Username is already in use'");
        exchange.setStatusCode(401);
        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.getResponseSender().send("");
    }

}
