package handler;

import com.google.gson.Gson;
import data.Data;
import data.User;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public abstract class RequestHandler implements HttpHandler {
    public abstract void handleRequest(HttpServerExchange exchange);

    protected Gson gson = new Gson();

    protected void answerOnError(HttpServerExchange exchange, int code) {
        exchange.setStatusCode(code);
        exchange.getResponseSender().send("");
    }

    protected String getToken(HttpServerExchange exchange) {
        if (exchange.getRequestHeaders().contains("Authorization")) {
            String token = exchange.getRequestHeaders().get("Authorization").getFirst();
            if (token.equals("")) return null;
            return token.replace("Token ", "");
        }
        else {
            return null;
        }
    }
}
