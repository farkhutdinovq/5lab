package handler;
import data.*;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class PostMessage extends RequestHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {

            String token = getToken(exchange);
            User user;
            if (token != null) {
                user = Data.getInstance().getUserByToken(token);
            }
            else {
                answerOnError(exchange, 401);
                return;
            }
            if (user != null) {
                Message message = readGson(data);
                if (message == null || message.getMessage() == null) {
                    answerOnError(exchange, 400);
                    return;
                }
              //  Message answer = Data.getInstance().addMessage(message.getMessage(), user.getId(), user.getName());
                //exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                //exchange.getResponseSender().send(gson.toJson(answer));
                user.updateLastAction();
            } else {
                answerOnError(exchange, 403);
            }
        });
    }

    private Message readGson(byte[] json) {
        try {
            return gson.fromJson(new String(json), Message.class);
        } catch (RuntimeException e) {
            return null;
        }
    }
}
