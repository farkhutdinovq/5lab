package handler;

import data.Data;
import data.GsonMessageList;
import data.User;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

import java.util.Deque;

public class GetMessages extends RequestHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {

            String token = getToken(exchange);
            User user;
            if (token != null) {
                user = Data.getInstance().getUserByToken(token);
            }
            else {
                answerOnError(exchange, 401);
                return;
            }
            if (user != null) {
                user.updateLastAction();
                int offset, count;
                try {
                    Deque<String> stringDeque = exchange.getQueryParameters().get("offset");
                    String sOffset = stringDeque != null ? stringDeque.getFirst() : "0";
                    offset = Integer.valueOf(sOffset);
                } catch (NumberFormatException exc) {
                    offset = 0;
                }
                try {
                    Deque<String> stringDeque = exchange.getQueryParameters().get("count");
                    String sCount = stringDeque != null ? stringDeque.getFirst() : "10";
                    count = Integer.valueOf(sCount);
                } catch (NumberFormatException exc) {
                    count = 10;
                }
                if (count > 100) {
                    count = 100;
                }
                exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                exchange.getResponseSender().send(gson.toJson(new GsonMessageList(Data.getInstance().getMessages(offset, count)),
                        GsonMessageList.class));
            } else {
                answerOnError(exchange, 403);
            }
        });
    }
}
