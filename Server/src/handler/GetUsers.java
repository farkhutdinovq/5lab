package handler;

import data.Data;
import data.User;
import data.GsonUserList;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

import java.util.List;
import java.util.stream.Collectors;

public class GetUsers extends RequestHandler {

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            if (ifUserId(exchange)) return;
            String token = getToken(exchange);
            User user;
            if (token != null) {
                user = Data.getInstance().getUserByToken(token);
            }
            else {
                answerOnError(exchange, 401);
                return;
            }
            if (user != null) {
                List<User> users = Data.getInstance().getUsers().stream()
                        .map((tUser) -> new User(tUser.getId(), tUser.getName(), tUser.isOnline()))
                        .collect(Collectors.toList());
                exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                exchange.getResponseSender().send(gson.toJson(new GsonUserList(users)));
                user.updateLastAction();
            } else {
                answerOnError(exchange, 403);
            }
        });
    }

    private boolean ifUserId(HttpServerExchange exchange) {
        try {
            String[] split = exchange.getRelativePath().split("/");
            if (split.length > 1) {
                System.out.println(split[0]);
                new GetUserID(split[1]).handleRequest(exchange);
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
