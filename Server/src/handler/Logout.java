package handler;

import data.*;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class Logout extends RequestHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            String token = getToken(exchange);
            User user;
            if (token != null) {
                user = Data.getInstance().getUserByToken(token);
            }
            else {
                answerOnError(exchange, 401);
                return;
            }
            if (user != null) {
                exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                exchange.getResponseSender().send(gson.toJson(new Message("bye!")));
                Data.getInstance().addServerMessage(user.getName() + " left the chat");
                Data.getInstance().removeUser(user);
            } else {
                answerOnError(exchange, 403);
            }
        });
    }
}
