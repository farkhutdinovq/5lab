package handler;

import data.Data;
import data.User;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class GetUserID extends RequestHandler {
    private int id;

    public GetUserID(String id) {
        try {
            this.id = Integer.valueOf(id);
        } catch (NumberFormatException ex) {
            this.id = -1;
        }
    }
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            String token = getToken(exchange);
            User user = null;
            if (null != token) {
                user = Data.getInstance().getUserByToken(token);
            }
            else {
                answerOnError(exchange, 401);
                return;
            }
            //User user = getUser(exchange);
            if (user != null) {
                exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                User requestedUser = Data.getInstance().getUserById(id);
                if (requestedUser != null) {
                    exchange.getResponseSender().
                            send(gson.toJson(new User(requestedUser.getId(), requestedUser.getName(), requestedUser.isOnline())));
                    user.updateLastAction();
                } else {
                    answerOnError(exchange, 404);
                }
            } else {
                answerOnError(exchange, 403);
            }
        });
    }
}
