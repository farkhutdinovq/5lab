package data;

import java.util.*;

public class Data {
    private static int userID = 1;
    private static int messageID = 1;
    private static volatile Data instance;
    private List<User> users;
    private List<Message> messages;
    private static MessageListener messageListener = null;

    //region Constructors
    private Data() {
        users = new ArrayList<>();
        messages = new ArrayList<>();
        //userAddresses = new HashMap<>();
    }

    public static Data getInstance() {
        if (instance == null) {
            synchronized (Data.class) {
                if (instance == null) {
                    instance = new Data();
                }
            }
        }
        return instance;
    }
    //endregion
    //region Getters
    public User getUserByToken(String token) {
        for (User user : users) {
            if (user.getToken().equals(token))
                return user;
        }
        return null;
    }

    public User getUserById(int id) {
        for (User user : users) {
            if (user.getId() == id)
                return user;
        }
        return null;
    }

    public List<Message> getMessages(int offset, int count) {
        return messages;
    }

    public List<User> getUsers() {
        return users;
    }
    //endregion
    //region Add functions
    public void addServerMessage(String text) {
        Message message = new Message(text, "Chat");
        message.setId(messageID);
        messageID++;
        messages.add(message);
        if (messageListener != null) {
            messageListener.send(message);
        }
    }

    public User addUser(String name) {
        for (User user: users) {
            if (user.getName().equals(name)) {
                user.setOnline();
                return user;
            }
        }
        User user = new User(name);
        user.setId(userID);
        userID++;
        users.add(user);
        return user;
    }

    public Message addMessage(String text, String authorName) {
        Message message = new Message(text, authorName);
        message.setId(messageID);
        messageID++;
        messages.add(message);
        return message;
    }
    //endregion
    //region Removers
    public void removeUser(User user) {
        users.remove(user);
    }
    public void removeUserByTimeOut(User user) {
        user.setOffline();
    }
    //endregion

    public static void setMessageListener(MessageListener messageListener) {
        Data.messageListener = messageListener;
    }

    public void setUserAddress(String userAddress, String userToken) {
        for (User user : users) {
            if (userToken.equals(user.getToken())) {
                user.setAddress(userAddress);
                break;
            }
        }
    }

    public void userDisconnected(String userAddress) {
        for (User user : users) {
            if (user.isOnline() && user.getAddress().equals(userAddress)) {
                user.setOffline();
                addServerMessage(user.getName() + " left the chat without logout");
            }
        }
    }


    public interface MessageListener {
        void send(Message message);
    }
}
