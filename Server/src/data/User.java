package data;

import java.util.UUID;

public class User {
    private static final int TIMEOUT = 500 * 1000;

    private int id;
    private String name;
    private boolean online;
    private String token;
    private transient long lastAction;
    private transient String address;

    //region Constructors
    public User(String name) {
        this.name = name;
        this.token = UUID.randomUUID().toString();
        online = true;
        lastAction = System.currentTimeMillis();
    }

    public User(int id, String name, boolean isOnline) { // for json
        this.name = name;
        this.id = id;
        this.online = isOnline;
    }

    public User(int id, String name, boolean isOnline, String token) {
        this.name = name;
        this.id = id;
        this.online = isOnline;
        this.token = token;
    }
    //endregion

    //region Getters
    public boolean isOnline() {
        return online;
    }

    public String getToken() {
        return token;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
    //endregion

    //region Setters
    void setId(int id) {
        this.id = id;
    }

    void setOffline() {
        online = false;
    }

    void setOnline() {
        online = true;
    }

    void setAddress(String address) {
        this.address = address;
    }
    //endregion

    public void updateLastAction() {
        lastAction = System.currentTimeMillis();
    }

    public boolean shouldBeKicked() {
        return System.currentTimeMillis() - lastAction >= TIMEOUT;
    }

    //region Equals
    @Override
    public boolean equals(Object o) {
        boolean withString = o instanceof String && o.equals(this.name);
        boolean withUser = o instanceof User && super.equals(o);
        return withString || withUser;
    }
    //endregion

}
