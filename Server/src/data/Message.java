package data;

public class Message {
    private String message;
    private int id;
    private int author;
    private String authorName;

    //region Constructors
    public Message(String text, int author, String authorName) {
        this.message = text;
        this.authorName = authorName;
        this.author = author;
    }

    public Message(String text, String authorName) {
        this.message = text;
        this.authorName = authorName;
    }

    public Message(String text) {
        this.message = text;
    }
    //endregion

    void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public String getAuthorName() {
        return authorName;
    }

}
