import java.net.URI;
import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) {
         if (args.length != 1) {
            System.out.println("try to write localhost:8080");
            return;
        }
        String url = args[0];
        try {
            new URI(url);
        } catch (URISyntaxException e) {
            System.out.println("Incorrect URL");
            return;
        }
        new Thread(new Client(url)).start();
    }
}
