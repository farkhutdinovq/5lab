package data;

public class Message {
    private String message;
    private int author;
    private int id;
    private String authorName;

    //region Constructors
    public Message(String message, int author, String authorName, int id) {
        this.message = message;
        this.author = author;
        this.authorName = authorName;
        this.id = id;
    }

    public Message(String message, String authorName) {
        this.message = message;
        this.authorName = authorName;
    }

    public Message(String text) {
        this.message = text;
    }
    //endregion
    //region Getters
    public String getMessage() {
        return message;
    }
    public int getAuthor() {
        return author;
    }
    public int getId() {
        return id;
    }
    public String getAuthorName() {
        return authorName;
    }
    //endregion
}
