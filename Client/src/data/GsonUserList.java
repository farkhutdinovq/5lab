package data;

import java.util.List;

public class GsonUserList {
    private List<User> users;

    public GsonUserList(List<User> users) {
        this.users = users;
    }
    public List<User> getUsers() {
        return users;
    }

}
