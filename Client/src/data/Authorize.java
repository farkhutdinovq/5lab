package data;

public class Authorize {
    private String username;

    public Authorize(String username) {
        this.username = username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }
}
