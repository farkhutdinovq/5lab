package data;

import java.util.List;

public class GsonMessageList {
    private List<Message> messages;

    public GsonMessageList(List<Message> messages) {
        this.messages = messages;
    }
    public List<Message> getMessages() {
        return messages;
    }

}
