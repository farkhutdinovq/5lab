package data;

public class User {
    private int id;
    private String name;
    private boolean online;
    private String token;

    //region Constructors
    public User(int id, String name, boolean isOnline) {
        this.id = id;
        this.name = name;
        this.online = isOnline;
    }

    public User() {
    }
    //endregion
    //region Setters
    public void setId(int id) {
        this.id = id;
    }
    public void setOnline(boolean online) {
        this.online = online;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setToken(String token) {
        this.token = token;
    }
    //endregion
    //region Getters
    public String getName() {
        return name;
    }
    public boolean isOnline() {
        return online;
    }
    public String getToken() {
        return token;
    }
    public int getId() {
        return id;
    }
    //endregion
}
