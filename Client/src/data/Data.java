package data;


import java.util.ArrayList;
import java.util.List;

public class Data {
    private static volatile Data instance;

    private int lastMessage = -1;
    private List<Message> messages;
    private User user;
    private List<User> users;

    //region Constructors
    private Data() {
        messages = new ArrayList<>();
        user = new User();
        users = new ArrayList<>();
    }

    public static Data getInstance() {
        if (instance == null) {
            synchronized (Data.class) {
                if (instance == null) {
                    instance = new Data();
                }
            }
        }
        return instance;
    }
    //endregion
    //region Getters
    public User getUser() {
        return user;
    }

    public List<Message> getMessages() {
        return messages;
    }
    //endregion
    //region Setters
    public void resetIndex() {
        if (lastMessage > -1) {
            lastMessage = -1;
            messages.clear();
        }
    }
    public void setUsers(List<User> users) {
        this.users.clear();
        this.users.addAll(users);
    }

    public void setUser(User user) {
        this.user = user;
    }
    //endregion
    //region Printers
    public void printNewMessages() {
       for (int i = lastMessage + 1; i < messages.size(); i++) {
            Message msg = messages.get(i);
            if (msg.getId() > lastMessage) {
                System.out.println(msg.getAuthorName() + ": " + msg.getMessage());
                lastMessage = i;
            }
        }
    }

    public void printUsers() {
        System.out.println("Online users");
        for (User user : users) {
            if (user.isOnline()) {
                System.out.println("  " + user.getName() + " " + user.getId() + " online");
            }
        }
    }

    public void printUser(User user) {
        System.out.println("username: " + user.getName());
        System.out.println("id: "  + user.getId());
        String isOnline = user.isOnline() ? "true" : "null";
        System.out.println("online : " + isOnline);
    }
    //endregion
}
