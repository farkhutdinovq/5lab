import com.google.gson.Gson;
import data.*;
import okhttp3.*;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Client implements Runnable {
    private RequestHandler requestHandler;
    private Gson gson;
    private WebSocket webSocket;
    private String name;
    private String token;

    public Client(String url) {
        this.requestHandler = new RequestHandler(url);
        gson = new Gson();
    }

    @Override
    public void run() {
        authenticate();
        Scanner scanner = new Scanner(System.in);

        openMessagesChannel();
        while (true) {
            String text = scanner.nextLine();
            if (text.equals("/logout")) {
                logout();
            } else if(text.equals("/users")) {
                Response response = null;
                try {
                    response = requestHandler.getUsers();
                    if (response.body() != null) {
                        String body = response.body().string();
                        List<User> users = gson.fromJson(body, GsonUserList.class).getUsers();
                        if (users.size() > 0) {
                            Data.getInstance().setUsers(users.stream()
                                    .map(user -> new User(
                                            user.getId(),
                                            user.getName(),
                                            user.isOnline()
                                    )).collect(Collectors.toList()));
                            response.body().close();
                        }
                        Data.getInstance().printUsers();
                    }
                } catch (IOException e) {
                    return;
                } finally {
                    if (response != null) {
                        response.close();
                    }
                }
            } else if (text.contains("/users/")) {
                getUserInfo(text);
            } else {
                webSocket.send(gson.toJson(new Message(text, name), Message.class));
            }
        }

    }

    private void authenticate() {
        Scanner scanner = new Scanner(System.in);
        boolean isLogin = false;
        Response response = null;
        while (!isLogin) {
            try {
                System.out.println("Login:");
                response = requestHandler.postLogin(scanner.nextLine(), gson);
                if (!response.headers("WWW-Authenticate").isEmpty() &&
                        response.headers("WWW-Authenticate").get(0).equals("Token realm='Username is already in use'")) {
                    System.out.println("Change your nickname. Such user exists");
                    continue;
                }
                if (response.body() != null) {
                    String body = response.body().string();
                    User user = gson.fromJson(body, User.class);
                    Data.getInstance().setUser(user);
                    name = user.getName();
                    token = user.getToken();
                    isLogin = true;
                    getHistory();
                }
            } catch (Exception e) {
                System.out.println("2Блин-блинский, опять сервер упал. Заходи заново");
                Data.getInstance().resetIndex();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    }

    private void openMessagesChannel() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("ws://localhost:8080/messages").header("Authorization", "Token " + token).get().build();
        MessagesWebSocketListener listener = new MessagesWebSocketListener();
        webSocket = client.newWebSocket(request, listener);
    }

    private void getUserInfo(String text) {
        Response response = null;
        String regex = "\\d+";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        if (m.find()) {
            try {
                response = requestHandler.getUser(Integer.parseInt(m.group()));
                if (response.body() != null) {
                    String body = response.body().string();
                    if (response.code() == 404)  {
                        System.out.println("User not found");
                        return;
                    }
                    User user = gson.fromJson(body, User.class);
                    Data.getInstance().printUser(user);
                    response.body().close();
                }

            } catch (IOException e) {
                System.out.println("3Блин-блинский, опять сервер упал. Заходи заново");
                Data.getInstance().resetIndex();
                authenticate();
            }
        } else {
            System.out.println("User not found");
        }
    }

    private void logout() {
        Response response = null;
        try {
            response = requestHandler.postLogout();
            String body = response.body().string();
            Message byeMessage = gson.fromJson(body, Message.class);
            System.out.println(byeMessage.getMessage());
            System.exit(0);
        } catch (IOException e) {
            System.out.println("4Блин-блинский, опять сервер упал. Заходи заново");
            Data.getInstance().resetIndex();
            authenticate();
        } finally {
            if (response != null && response.body() != null) {
                response.body().close();
            }
        }
    }

    private void postMessage(String text) {
        Response response = null;
        try {
            response = requestHandler.postMessage(text, gson);
        } catch (IOException e) {
            System.out.println("5Блин-блинский, опять сервер упал. Заходи заново");
            Data.getInstance().resetIndex();
            authenticate();
        } finally {
            if (response != null && response.body() != null) {
                response.body().close();
            }
        }
    }

    private void getHistory() {
        Response response = null;
        try {
            response = requestHandler.getMessages(0, 100);
            if (response.body() != null) {
                String s = response.body().string();
                List<Message> messages = gson.fromJson(s, GsonMessageList.class).getMessages();
                if (messages.size() > 0) {

                    Data.getInstance().getMessages().addAll(messages.stream()
                            .map(messageObject -> new Message(
                                    messageObject.getMessage(),
                                    messageObject.getAuthor(),
                                    messageObject.getAuthorName(),
                                    messageObject.getId()
                            )).collect(Collectors.toList()));
                    Data.getInstance().printNewMessages();
                    response.body().close();
                }
            }
            response.body().close();
        } catch (Exception ex) {
            System.out.println("1Блин-блинский, опять сервер упал. Заходи заново");
            Data.getInstance().resetIndex();
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    private final class MessagesWebSocketListener extends WebSocketListener {

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Message message = gson.fromJson(text, Message.class);
            System.out.println(message.getAuthorName() + ": " + message.getMessage());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            System.out.println("close");
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            System.out.println("Error: " + t.getMessage());
            System.exit(0);
        }
    }

}
