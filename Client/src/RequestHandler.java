import com.google.gson.Gson;
import data.Authorize;
import data.Data;
import data.Message;
import okhttp3.*;

import java.io.IOException;
import java.net.ConnectException;

public class RequestHandler {
    private OkHttpClient okhttp;
    private String url;

    public RequestHandler(String url) {
        this.url = url;
        okhttp = new OkHttpClient();
    }

    //region POST
    public Response postLogin(String username, Gson gson) throws IOException {
        String body = gson.toJson(new Authorize(username));
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);
        Request request = new Request.Builder().url("http://" + url + "/login").method("POST", requestBody).
                build();
        try {
            return okhttp.newCall(request).execute();
        } catch (ConnectException e) {
            return null;
        }
    }

    public Response postMessage(String message, Gson gson) throws IOException{
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), gson.toJson(new Message(message), Message.class));
        Request request = new Request.Builder().
                url("http://" + url + "/messages").
                header("Authorization", "Token " + Data.getInstance().getUser().getToken()).method("POST", body).
                build();
        return okhttp.newCall(request).execute();
    }

    public Response postLogout() throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), "");
        Request request = new Request.Builder().
                url("http://" + url + "/logout").
                header("Authorization", "Token " + Data.getInstance().getUser().getToken()).method("POST", body).
                build();
        return okhttp.newCall(request).execute();
    }
    //endregion
    //region GET
    public Response getMessages(int offset, int count) throws IOException {
        Request request = new Request.Builder().
                url("http://" + url + "/messages?offset=" + offset + "&count=" + count).
                        header("Authorization", "Token " + Data.getInstance().getUser().getToken()).method("GET", null).
                build();
        return okhttp.newCall(request).execute();
    }

    public Response getUsers() throws IOException {
        Request request = new Request.Builder().
                url("http://" + url + "/users").
                header("Authorization", "Token " + Data.getInstance().getUser().getToken()).method("GET", null).
                build();
        return okhttp.newCall(request).execute();
    }

    public Response getUser(int id) throws IOException {
        Request request = new Request.Builder().
                url("http://" + url + "/users/" + id).
                header("Authorization", "Token " + Data.getInstance().getUser().getToken()).method("GET", null).
                build();
        return okhttp.newCall(request).execute();
    }
    //endregion
}
